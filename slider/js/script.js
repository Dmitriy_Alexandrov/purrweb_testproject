'use strict';

var imgCnt = 5;
var images = document.getElementsByClassName("image"); // все картинки
var microbuttonimgs = document.getElementsByClassName("microButton_img");

let curPos = 0;
let prevPos = 0;
let leftIsbusy = false;


function animateLeft(left, right) {
    let leftObj = images[left]; // Новая картинка
    let rightObj = images[right]; // Старая картинка
    let done_cnt = 0;
    let force = 0;
    leftObj.style.left = leftObj.style.right - 1000 + "px";
    leftObj.style.display = "block"; // новая картинка

    let rightMicrobutton = microbuttonimgs[prevPos];
    let leftMicrobutton = microbuttonimgs[curPos];
    

    let timer = setInterval(function() {
        rightObj.style.left = 0 + force + 'px';
        leftObj.style.left = -1000 + force + "px";
        force += 50;
        done_cnt++;
        if (done_cnt >= 21) {
            clearInterval(timer);
            rightObj.style.left = 0; // старая картинка
            rightObj.style.display = "none"; // старая картинка
            done_cnt = 0;
            leftMicrobutton.src = "/img/circle.svg";
            rightMicrobutton.src = "/img/circle_empty.svg";
            leftIsbusy = false;
            return;
        }
    }, 20);
    force = 0;
};

function animateRight(left, right) {
    let leftObj = images[left]; // Новая картинка
    let rightObj = images[right]; // Старая картинка
    let done_cnt = 0;
    let force = 0;
    rightObj.style.left = leftObj.style.right + 1000 + "px";
    rightObj.style.display = "block"; // новая картинка

    let timer = setInterval(function() {
        rightObj.style.left = 1000 - force + 'px';
        leftObj.style.left = - force + "px";
        force += 50;
        done_cnt++;

        let rightMicrobutton = microbuttonimgs[prevPos];
        let leftMicrobutton = microbuttonimgs[curPos];
        if (done_cnt >= 21) {
            clearInterval(timer);
            leftObj.style.left = 0; // старая картинка
            leftObj.style.display = "none"; // старая картинка
            done_cnt = 0;
            leftMicrobutton.src = "/img/circle.svg";
            rightMicrobutton.src = "/img/circle_empty.svg";
            leftIsbusy = false;
            return;
        }
    }, 20);
    force = 0;
};

function leftButtonPress() {
    if (!leftIsbusy) {
        leftIsbusy = true;
        prevPos = curPos;
        curPos = curPos - 1;
        if (curPos < 0) {
            curPos = imgCnt -1;
        }
        animateLeft(curPos, prevPos);
    }
};

function rightButtonPress() {
    if (!leftIsbusy) {
        leftIsbusy = true;
        prevPos = curPos;
        curPos = curPos + 1;
        if (curPos >= imgCnt) {
            curPos = 0;
        }
        animateRight(prevPos, curPos);
    }
};

function microButtonPress(num) {
    if (!leftIsbusy) {
        if (curPos != num) {
            leftIsbusy = true;
            if (num > curPos) { // Направо
                prevPos = curPos;
                curPos = num;
                animateRight(prevPos, curPos);
            } else { // Налево
                prevPos = curPos;
                curPos = num;
                animateLeft(curPos, prevPos);
            }
        }
    }
};
